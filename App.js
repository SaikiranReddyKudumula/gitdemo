import React, { Component } from "react";
import {
  Text,
  View,
  Image,
  StyleSheet,
  TouchableWithoutFeedback
} from "react-native";
import Home from "../gitDemo/src/screens/Home";
import AppNavigator from "./src/navigation/index.js"

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }


  render() {
    return <AppNavigator />;
  }
}

export default App;
