/**
 * @format
 */
import React, { Component } from "react";
import 'react-native-gesture-handler';
import { AppRegistry } from 'react-native';
import App from './App'
import { name as appName } from './app.json';
import Store from "./src/utils/Store"
import { Provider } from "react-redux";
const Wrapper = () => (
    <Provider store={Store}>
        <App />
    </Provider>
);
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => Wrapper);
