import { DotsLoader } from 'react-native-indicator';
import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    Animated, TouchableOpacity
} from "react-native";

import Spinner from 'react-native-loading-spinner-overlay';

class Loader extends Component {
    render() {
        return (
            <Spinner
                visible={false}
            />
        );
    }

}
export default Loader
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white"
    },
})