import {
    GET_SEARCH_DETAILS, GET_SEARCH_DETAILS_SUCCESSFULL,
    CLEAR_SEARCH_DETAILS, SET_REPO_DETAILS,
    GET_CONTRIBUTOR_DETAILS, GET_CONTRIBUTOR_PROJECT_DETAILS,
    CLEAR_REPO_DETAILS, CLEAR_CONTRIBUTOR_DETAILS, CLEAR_CONTRIBUTOR_PROJECT_DETAILS,
    SET_CONTRIBUTOR_DETAILS, SET_CONTRIBUTOR_PROJECT_DETAILS, GET_CONTRIBUTOR_DETAILS_START, GET_CONTRIBUTOR_PROJECT_DETAILS_START
} from "./actionType"
import axiosInstance from "../utils/axiosInstance"





export const getData = payload => async dispatch => {
    try {
        dispatch({
            type: GET_SEARCH_DETAILS,

        });
        const response = await axiosInstance.get(`/search/repositories?q=${payload}&sort=stars&order=desc`);

        if (response.status === 200) {
            // debugger;
            let payload = response.data.items
            if (payload.length > 10) {
                payload = response.data.items.slice(1, 11)
            }
            dispatch({
                type: GET_SEARCH_DETAILS_SUCCESSFULL,
                payload: payload
            });
        }

    }
    catch (e) {
        console.error(e)
    }
}
export const clearData = () => async dispatch => {
    try {
        dispatch({
            type: CLEAR_SEARCH_DETAILS,
            payload: null

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const setRepo = (payload) => async dispatch => {


    try {
        dispatch({
            type: SET_REPO_DETAILS,
            payload: payload

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const clearRepo = () => async dispatch => {
    try {
        dispatch({
            type: CLEAR_REPO_DETAILS,
            payload: null

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const getContributorDetails = (payload) => async dispatch => {
    try {

        dispatch({
            type: GET_CONTRIBUTOR_DETAILS_START,


        });
        fetch(payload)
            .then(response => response.json())
            .then((responseJson) => {
                dispatch({
                    type: GET_CONTRIBUTOR_DETAILS,
                    payload: responseJson

                });
            })
            .catch(error => console.log(error)) //to catch the errors if any



    }
    catch (e) {
        dispatch({
            type: GET_CONTRIBUTOR_DETAILS_START,


        });
        console.error(e)
    }
}
export const setContributorDetails = (payload) => async dispatch => {

    try {
        dispatch({
            type: SET_CONTRIBUTOR_DETAILS,
            payload: payload

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const clearContributorDetails = () => async dispatch => {
    try {
        dispatch({
            type: CLEAR_CONTRIBUTOR_DETAILS,
            payload: null

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const getContributorProjectDetails = (payload) => async dispatch => {
    try {

        dispatch({
            type: GET_CONTRIBUTOR_PROJECT_DETAILS_START,


        });

        fetch(payload)
            .then(response => response.json())
            .then((responseJson) => {
                dispatch({
                    type: GET_CONTRIBUTOR_PROJECT_DETAILS,
                    payload: responseJson

                });
            })
            .catch(error => console.log(error)) //to catch the errors if any



    }
    catch (e) {
        dispatch({
            type: GET_CONTRIBUTOR_PROJECT_DETAILS_START,


        });

        console.error(e)
    }
}
export const setContributorProjectDetails = (payload) => async dispatch => {
    try {
        dispatch({
            type: SET_CONTRIBUTOR_PROJECT_DETAILS,
            payload: payload

        });


    }
    catch (e) {
        console.error(e)
    }
}
export const clearContributorProjectDetails = () => async dispatch => {
    try {
        dispatch({
            type: CLEAR_CONTRIBUTOR_PROJECT_DETAILS,
            payload: null

        });


    }
    catch (e) {
        console.error(e)
    }
}