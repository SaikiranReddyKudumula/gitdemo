import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    SafeAreaView,
    TextInput,
    Platform,
    StatusBar,
    ScrollView,
    Image,
    Dimensions,
    Animated, TouchableOpacity
} from "react-native";
import { Icon } from 'react-native-elements'
import { getData, clearData, setRepo } from '../actions/git'
import { connect } from 'react-redux';
import { FlatList } from "react-native-gesture-handler";
import Modal from "react-native-modalbox";
import Loader from "../components/Loader";
const { height, width } = Dimensions.get('window')
class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            screenText: "Please search any github repo",
            text: "",
            filter: false,
            languagesModal: false,
            color: "white",
            requiredLanguages: [],
            finalLanguageFilters: [],
            filteredData: [],
            isApplied: false,
            filterByAlphabets: false
        }
    }
    componentWillMount() {

        this.scrollY = new Animated.Value(0)

        this.startHeaderHeight = 80
        this.endHeaderHeight = 50
        if (Platform.OS == 'android') {
            this.startHeaderHeight = 100 + StatusBar.currentHeight
            this.endHeaderHeight = 70 + StatusBar.currentHeight
        }

        this.animatedHeaderHeight = this.scrollY.interpolate({
            inputRange: [0, 50],
            outputRange: [this.startHeaderHeight, this.endHeaderHeight],
            extrapolate: 'clamp'
        })

        this.animatedOpacity = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [0, 1],
            extrapolate: 'clamp'
        })
        this.animatedTagTop = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [-30, 10],
            extrapolate: 'clamp'
        })
        this.animatedMarginTop = this.animatedHeaderHeight.interpolate({
            inputRange: [this.endHeaderHeight, this.startHeaderHeight],
            outputRange: [50, 30],
            extrapolate: 'clamp'
        })


    }
    findData = (text) => {
        if (text == '') {
            this.props.clearData()
            this.setState({
                screenText: "Please search any github repo",
                text: "",
                filteredData: [],
                requiredLanguages: [],
                isApplied: false,
                filterByAlphabets: false
            })
            return
        }
        this.setState({ text: text, screenText: "", requiredLanguages: [], filteredData: [], isApplied: false, filterByAlphabets: false })

        setTimeout(() => {

            this.props.getData(text)
        }, 500)

    }
    open = () => {
        this.setState({ filter: true })
    }
    close = () => {
        this.setState({ filter: false })
    }
    navigate = () => {

        this.props.navigation.push("repo")
        this.props.setRepo(item)
    }
    filterArr = (data, key) => {
        return data.reduce((result, current) => {
            if (!result[current[key]]) {
                result[current[key]] = 1;
            } else {
                result[current[key]] += 1;
            }
            return console.log(result)
        }, {})
    }
    sortByLanguage = () => {
        let a = this.props.repoData
        console.log(a)
        let languageList = []
        a.filter((e) => {
            if (e.language != null)
                languageList.push(e.language)
        })
        let counts = {};
        languageList.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
        this.setState({
            counts,
            languagesModal: true
        })
    }
    renderItem = ({ item, index }) => {

        return (
            <TouchableOpacity style={{ flex: 1 }} onPress={() => {

                this.props.navigation.push("repo")
                this.props.setRepo(item)
            }}>
                <View style={{ flexDirection: "row", margin: 10, }}>
                    <Image source={{ uri: item.owner.avatar_url }} style={{ width: 150, height: 150, borderRadius: 15 }} resizeMode="contain" />
                    <View style={{ flexDirection: "column", }}>
                        <Text style={{ fontWeight: "bold", padding: 5, width: width / 2 }}>{item.name}</Text>
                        <Text style={{ padding: 10, width: width / 2 }}>{item.full_name}</Text>
                        <Text style={{ padding: 10, color: "#D3D3D3" }}>{item.watchers_count} Watchers</Text>

                    </View>
                </View>
                {this.props.loading ? <Loader /> : null}
            </TouchableOpacity>
        )
    }

    closeLanguagesModal = () => {

        this.setState({
            languagesModal: false
        })
    }
    sortByAlphabets = () => {
        this.setState({
            filterByAlphabets: !this.state.filterByAlphabets
        })
    }
    applyFilter = () => {
        let a = this.props.repoData

        let { requiredLanguages, filterByAlphabets } = this.state
        if (requiredLanguages.length > 0 && !filterByAlphabets) {
            let b = []
            a.map(item => {
                requiredLanguages.map((e) => {
                    if (item.language == e) {
                        b.push(item)
                    }
                })
            }

            )
            this.setState({ filteredData: b, isApplied: true, filter: false })
        }
        if (!requiredLanguages.length > 0 && filterByAlphabets) {

            // console.log("Before Sort", a)
            let c = a.sort(function (a, b) {
                let x = a.name.toLowerCase();
                let y = b.name.toLowerCase();
                if (x < y) { return -1; }
                if (x > y) { return 1; }
                return 0;
            });
            // console.log("After sort", c)
            this.setState({ filteredData: c, isApplied: true, filter: false })
        }
        if (requiredLanguages.length > 0 && filterByAlphabets) {
            let x = []
            // console.log("Before Sort", a)
            let c = a.sort(function (a, b) {
                let x = a.name.toLowerCase();
                let y = b.name.toLowerCase();
                if (x < y) { return -1; }
                if (x > y) { return 1; }
                return 0;
            });
            c.map(item => {
                requiredLanguages.map((e) => {
                    if (item.language == e) {
                        x.push(item)
                    }
                })
            }

            )
            // console.log("After sort", c)
            this.setState({ filteredData: x, isApplied: true, filter: false })
        }
        if (!requiredLanguages.length > 0 && !filterByAlphabets) {

            // console.log("After sort", c)
            this.setState({ filteredData: [], isApplied: false, filter: true })
        }





    }
    render() {
        let { filter, requiredLanguages, filterByAlphabets, languagesModal, counts, text, isApplied, filteredData, screenText } = this.state

        return (
            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ flex: 1 }}>

                    <Modal style={{
                        height: height / 4, margin: 0
                        // flex: 1, justifyContent: "flex-end", alignItems: "flex-end",
                        // backgroundColor: "white", height: 300
                    }} position={"bottom"} isOpen={filter} onClosed={this.close}
                    >
                        <View style={{ backgroundColor: "white", justifyContent: "flex-start", alignItems: "flex-start", margin: 20, }}>
                            <View style={{ flexDirection: 'row', }}>
                                <Text style={{ fontWeight: "bold", fontSize: 30, }}>Filter by</Text>
                                <Text style={{ fontSize: 25, color: "#0064bd", padding: 5, marginLeft: width / 3, width: "100%" }} onPress={this.applyFilter} >Apply</Text>
                            </View>
                            {filterByAlphabets > 0 ? <Text style={{ fontSize: 25, padding: 15, color: "green" }} onPress={this.sortByAlphabets}>Alphabets(A-Z)</Text> : <Text style={{ fontSize: 20, padding: 15 }} onPress={this.sortByAlphabets}>Alphabets</Text>}
                            {requiredLanguages.length > 0 ? <Text style={{ fontSize: 25, padding: 15, color: "green" }} onPress={this.sortByLanguage} >Language({requiredLanguages.length}) ></Text> : <Text style={{ fontSize: 20, padding: 15 }} onPress={this.sortByLanguage} >Language</Text>}


                        </View>
                    </Modal>
                    <Modal style={{
                        height: height / 4
                        // flex: 1, justifyContent: "flex-end", alignItems: "flex-end",
                        // backgroundColor: "white", height: 300
                    }} position={"bottom"} isOpen={languagesModal} onClosed={this.closeLanguagesModal}
                    >
                        <View style={{ backgroundColor: "white", justifyContent: "flex-start", alignItems: "flex-start", margin: 20, }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Text style={{ fontWeight: "bold", fontSize: 30, }}>Available Languages</Text>
                                <Text style={{ fontSize: 20, color: "#0064bd", marginTop: 5, marginLeft: width / 12 }}
                                    onPress={this.closeLanguagesModal}
                                >Done</Text>
                            </View>
                            <View style={{ flexDirection: "row", margin: 10, flexWrap: "wrap" }}>
                                {counts && Object.keys(counts).map((e) => (
                                    <Text style={[styles.text, { backgroundColor: `${requiredLanguages.includes(e) ? "#00ff00" : "white"}` }]} onPress={() => {
                                        if (requiredLanguages.includes(e)) {
                                            this.setState((prevState) => ({
                                                requiredLanguages: [...prevState.requiredLanguages.filter(item => item !== e)]
                                            }))
                                        } else {
                                            this.setState((prevState) => ({
                                                requiredLanguages: [...prevState.requiredLanguages, e]
                                            }))
                                        }
                                    }}>{e}({counts[e]})</Text>

                                ))}
                            </View>


                        </View>
                    </Modal>


                    <Animated.View style={{ height: this.animatedHeaderHeight, backgroundColor: 'white', borderBottomWidth: 1, borderBottomColor: '#dddddd' }}>
                        <View style={{
                            flexDirection: 'row', padding: 10,
                            backgroundColor: 'white', marginHorizontal: 20,
                            shadowOffset: { width: 0, height: 0 },
                            shadowColor: 'black',
                            shadowOpacity: 0.2,
                            elevation: 1,
                            marginTop: Platform.OS == 'android' ? 30 : null
                        }}>

                            <TextInput
                                underlineColorAndroid="transparent"
                                placeholder="Search..."
                                placeholderTextColor="grey"
                                style={{ flex: 1, fontWeight: '700', backgroundColor: 'white' }}
                                clearButtonMode="while-editing"
                                onChangeText={this.findData}
                                value={text}
                            />
                            {text ? <Icon name={"filter"} onPress={this.open} type="feather" size={30} color='#696969' /> : null}
                        </View>
                        {isApplied && <Animated.View
                            style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: "center", top: this.animatedTagTop, opacity: this.animatedOpacity }}
                        >
                            <Text>Filters:</Text>
                            {requiredLanguages.length > 0 ? requiredLanguages.map(

                                e => (
                                    <View style={{ marginRight: 3 }}>
                                        <Text style={styles.text1}>{e}</Text>
                                    </View>
                                )) : null}
                        </Animated.View>}

                    </Animated.View>

                    {this.props.repoData ?
                        <View style={{ backgroundColor: 'white', flex: 1 }}>
                            <FlatList
                                removeClippedSubviews={true}
                                data={filteredData.length > 0 ? filteredData : this.props.repoData}
                                renderItem={this.renderItem}
                                numColumns={1}
                                initialNumToRender={2}
                                scrollEnabled={true}
                            /></View> : <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center',
                                backgroundColor: 'white'
                            }}><Text style={{ color: "#DA70D6" }}>{screenText}</Text></View>}




                </View>
                {this.props.loading ? <Loader /> : null}
            </SafeAreaView>

        );
    }
}
const mapStateToProps = state => ({
    repoData: state.repoData.repoData,
    loading: state.repoData.loading,


});


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "white"
    },
    contentContainer: {
        marginTop: 50,
        paddingVertical: 20,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        flex: 1,
        margin: 20,
        backgroundColor: 'orange',
        margin: 10,
        textAlign: 'center',
        fontSize: 20,
        paddingTop: 70,
    },
    view: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        margin: 0,


    },
    text: {
        fontSize: 15, padding: 10, borderWidth: 2,
        // backgroundColor: "white",
        borderColor: "white"
    },
    text1: {
        fontSize: 10, padding: 5, borderWidth: 1,
        backgroundColor: "#07c",
        borderColor: "white",
        color: "white",
    }
});
export default (

    connect(
        mapStateToProps,
        { getData, clearData, setRepo }
    )(Home)

);
