import React, { Component } from "react";
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableWithoutFeedback, Dimensions, Linking, BackHandler
} from "react-native";
import { setRepo, getContributorDetails, getContributorProjectDetails, clearRepo, setContributorDetails } from '../actions/git'
import { connect } from 'react-redux';
import { ScrollView, FlatList, TouchableOpacity } from "react-native-gesture-handler";
import { awaitExpression } from "@babel/types";
import Loader from "../components/Loader"
const { height, width } = Dimensions.get('window')

class RepoDetails extends Component {
    static navigationOptions = ({ navigation, screenProps }) => {
        const { params } = navigation.state;

        return {

            title: '',
        };

    };
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            html_url: "",
            description: "",
            avatar_url: "",
        };
    }

    componentDidMount() {

        this.props.getContributorDetails(this.props.repo.contributors_url)
        const { navigation } = this.props;

        this.focusListener = navigation.addListener('didFocus', async () => {

            this.props.navigation.setParams({ title: this.props.repo.name });
            this.setState({
                name: this.props.repo.name,
                html_url: this.props.repo.html_url,
                description: this.props.repo.description,
                avatar_url: this.props.repo.owner.avatar_url

            })




        });



    }
    componentWillUnmount() {
        // Remove the event listener
        this.focusListener.remove();

    }
    handleBackPress = () => {
        this.props.navigation.navigate("home")
        return true;
    }
    renderItem = ({ item }) => {
        return (<TouchableOpacity style={{}} onPress={async () => {
            // this.props.clearRepo()
            this.props.navigation.setParams({ title: item.login });
            this.props.setContributorDetails(item)
            // await this.props.getContributorProjectDetails(item.repos_url)

            this.props.navigation.push("contribute")

        }} >
            <Image source={{ uri: item.avatar_url }} style={{ width: 140, height: 140, borderRadius: 15, margin: 10 }} />
        </TouchableOpacity>)

    }

    render() {
        let { name, html_url, description, avatar_url } = this.state
        console.log("Navigation Params", this.props.navigation)
        let contributors = []
        contributors = this.props.contributorDetails

        return (
            <ScrollView style={{ backgroundColor: "white" }} showsHorizontalScrollIndicator={true} scrollsToTop={true}>
                <View style={styles.container}>
                    <Loader />

                    <Text style={{ fontWeight: "bold", padding: 20, fontSize: 40 }}>{name}</Text>
                    <View style={{
                        // marginLeft: width / 4,
                        margin: 10,
                    }}>
                        <Image source={{ uri: avatar_url }} style={{ width: width / 1.1, height: width / 1.7, borderRadius: 15, }} />
                        <View style={{ margin: 10 }}>
                            <View style={{ flexDirection: "row", margin: 5 }}>
                                <Text style={{ width: "30%" }}>Name:</Text>
                                <Text>{name}</Text>
                            </View>
                            <View style={{ flexDirection: "row", margin: 5 }}>
                                <Text style={{ width: "30%" }}>Project Link:</Text>
                                <Text style={{ color: 'blue', width: "70%" }} onPress={() => Linking.openURL(html_url)}>{html_url}</Text>
                            </View>
                            <View style={{ flexDirection: "row", margin: 5, }}>
                                <Text style={{ width: "30%" }}>Description:</Text>
                                <Text style={{ width: "70%" }}>{description}</Text>
                            </View>
                        </View>
                    </View>
                    <Text style={{ fontWeight: "bold", padding: 20, fontSize: 40 }}>Contributors</Text>
                    {contributors != null ? <FlatList
                        removeClippedSubviews={true}
                        data={contributors}
                        renderItem={this.renderItem}
                        numColumns={2}
                        initialNumToRender={10}
                        scrollEnabled={true}
                        contentContainerStyle={{ alignItems: "center" }}
                    /> : <Text style={{ color: "red" }}>No Contributors</Text>}

                </View>
                {this.props.loading ? <Loader /> : null}
            </ScrollView>
        )
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,

        backgroundColor: "white"
    },
})
const mapStateToProps = state => ({
    repo: state.repoData.repo,
    loading: state.repoData.loading,
    contributorDetails: state.repoData.contributorDetails,

});

export default (
    connect(
        mapStateToProps,
        { setRepo, clearRepo, getContributorDetails, getContributorProjectDetails, setContributorDetails }
    )(RepoDetails)

);
