import React, { Component } from "react";
import {
    Text,
    View,
    Image,
    StyleSheet,
    TouchableWithoutFeedback, Dimensions, ScrollView, TouchableOpacity, BackHandler
} from "react-native";
import { setRepo, getContributorDetails, clearContributorDetails, clearContributorProjectDetails, getContributorProjectDetails } from '../actions/git'
import { connect } from 'react-redux';
import { FlatList } from "react-native-gesture-handler";
import Loader from "../components/Loader"
import { HeaderBackButton } from "react-navigation-stack";
import { Header } from "react-native/Libraries/NewAppScreen";
import { Icon } from 'react-native-elements'
const { height, width } = Dimensions.get('window')

class ContributorDetails extends Component {

    static navigationOptions = ({ navigation, screenProps }) => {
        const { params } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flexDirection: "row", marginLeft: 10 }}>
                    <Icon name={"ios-arrow-back"} onPress={() => { navigation.pop() }} type="ionicon" size={30} color='#07c' iconStyle={{ marginRight: 10 }} />
                    <Text onPress={() => { navigation.pop() }} style={{ fontSize: 20, color: "#07c", marginTop: 3 }}>{params ? params.title : ''}</Text>
                </View>),
            title: '',


        };

    };
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            avatar: ""
        };
    }

    componentDidMount() {

        console.log("Contributer Project details", this.props.currentContributorDetail)

        this.props.getContributorProjectDetails(this.props.currentContributorDetail.repos_url)

        const { navigation } = this.props;


        this.props.navigation.setParams({ title: this.props.currentContributorDetail.login });



    }

    renderItem = ({ item, index }) => {

        return (
            <TouchableOpacity style={{ flex: 1 }} onPress={() => {
                this.props.setRepo(item)
                // this.props.clearContributorProjectDetails()
                this.props.navigation.push("repo")
            }}>
                <View style={{ flexDirection: "row", margin: 20 }}>
                    <Image source={{ uri: item.owner.avatar_url }} style={{ width: 150, height: 150, borderRadius: 15 }} resizeMode="contain" />
                    <View style={{ flexDirection: "column", margin: 10, flexWrap: "wrap" }}>
                        <Text style={{ fontWeight: "bold", padding: 10, width: "90%" }}>{item.name}</Text>
                        <Text style={{ padding: 10, width: "90%" }}>{item.full_name}</Text>
                        <Text style={{ padding: 10, color: "#D3D3D3" }}>{item.watchers_count} Watchers</Text>

                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        console.log("Navigation Params", this.props.navigation)
        console.log("Details in render", this.props.currentContributorDetail)
        let { avatar_url, login } = this.props.currentContributorDetail
        // let { avatar_url } = this.props.contributorProjectDetails[0].owner
        return (
            <ScrollView style={styles.container} showsHorizontalScrollIndicator={true}>

                {this.props.currentContributorDetail ? <Text style={{ fontWeight: "bold", padding: 20, fontSize: 35 }}>{login}</Text> : null}
                {this.props.currentContributorDetail ? <View style={{
                    // marginLeft: width / 4,
                    margin: 20,
                }}>
                    <Image source={{ uri: avatar_url }} style={{ width: width / 1.1, height: width / 1.7, borderRadius: 15, }} />
                </View> : null}
                <Text style={{ fontWeight: "bold", padding: 20, fontSize: 40 }}>Repo List</Text>


                <FlatList
                    removeClippedSubviews={true}
                    data={this.props.contributorProjectDetails}
                    renderItem={this.renderItem}
                    numColumns={1}
                    initialNumToRender={10}
                    scrollEnabled={true}
                />

                {this.props.loading ? <Loader /> : null}
            </ScrollView>
        )
    }
}

const mapStateToProps = state => ({
    contributorProjectDetails: state.repoData.contributorProjectDetails,
    repo: state.repoData.repo,
    loading: state.repoData.loading,
    currentContributorDetail: state.repoData.currentContributorDetail


});
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
})

export default (
    connect(
        mapStateToProps,
        { setRepo, clearContributorDetails, clearContributorProjectDetails, getContributorProjectDetails }
    )(ContributorDetails)

);
