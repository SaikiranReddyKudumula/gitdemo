import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Home from "../screens/Home"
import RepoDetails from "../screens/RepoDetails"
import ContributorDetails from "../screens/ContributorDetails"
const AppNavigator = createStackNavigator(
    {
        home: {
            screen: Home,
            navigationOptions: { header: null },
        },
        repo: {
            screen: RepoDetails,
            navigationOptions: {
                // header: null,
                headerBackTitle: 'GitRepo'
            },
        },
        contribute: {
            screen: ContributorDetails,
            // navigationOptions: { header: null },
        },


    },
    {
        initialRouteName: "home",
        header: null,
        // transitionConfig: NavigationConfig
        // transparentCard: true
    }
);
export default createAppContainer(AppNavigator);
