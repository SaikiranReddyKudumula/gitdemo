import repoData from './git';

import { combineReducers } from 'redux';
const rootReducer = combineReducers({
    repoData
});

export default rootReducer;
