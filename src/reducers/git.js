
import {
    GET_SEARCH_DETAILS, GET_SEARCH_DETAILS_SUCCESSFULL,
    CLEAR_SEARCH_DETAILS, SET_REPO_DETAILS,
    GET_CONTRIBUTOR_DETAILS, GET_CONTRIBUTOR_PROJECT_DETAILS,
    CLEAR_REPO_DETAILS, CLEAR_CONTRIBUTOR_DETAILS, CLEAR_CONTRIBUTOR_PROJECT_DETAILS,
    SET_CONTRIBUTOR_DETAILS, SET_CONTRIBUTOR_PROJECT_DETAILS, GET_CONTRIBUTOR_DETAILS_START, GET_CONTRIBUTOR_PROJECT_DETAILS_START
} from "../actions/actionType"

const initialState = {

    loading: false,
    repoData: null,
    repo: null,
    contributorDetails: null,
    currentContributorDetail: null,
    contributorProjectDetails: null,
    currentContributorProjectDetail: null,
    logOut: false
};
const repoReducer = (state = initialState, action) => {
    const { type, payload } = action;
    switch (type) {
        case GET_SEARCH_DETAILS: {
            return {
                ...state,
                loading: true,

            };
        }
        case GET_SEARCH_DETAILS_SUCCESSFULL: {
            return {
                ...state,
                loading: false,
                repoData: payload
            };
        }
        case CLEAR_SEARCH_DETAILS: {
            return {

                repoData: payload
            };
        }
        case SET_REPO_DETAILS: {
            return {
                ...state,
                repo: payload
            };
        }
        case CLEAR_REPO_DETAILS: {
            return {
                ...state,
                repo: payload
            };
        }
        case GET_CONTRIBUTOR_DETAILS: {
            return {
                ...state,
                loading: !{ ...state }.loading,
                contributorDetails: payload,

            };
        }
        case GET_CONTRIBUTOR_DETAILS_START: {
            return {
                ...state,
                loading: !{ ...state }.loading
            };
        }

        case SET_CONTRIBUTOR_DETAILS: {

            return {
                ...state,
                currentContributorDetail: payload
            };
        }
        case CLEAR_CONTRIBUTOR_DETAILS: {
            return {
                ...state,
                contributorDetails: payload
            };
        }
        case GET_CONTRIBUTOR_PROJECT_DETAILS: {
            return {
                ...state,
                loading: !{ ...state }.loading,
                contributorProjectDetails: payload,

            };
        }

        case GET_CONTRIBUTOR_PROJECT_DETAILS_START: {
            return {
                ...state,
                loading: !{ ...state }.loading
            };
        }
        case SET_CONTRIBUTOR_PROJECT_DETAILS: {
            return {
                ...state,
                currentContributorProjectDetail: payload
            };
        }
        case CLEAR_CONTRIBUTOR_PROJECT_DETAILS: {
            return {
                ...state,
                contributorProjectDetails: payload
            };
        }


        default:
            return state;



    }
}
export default repoReducer;
